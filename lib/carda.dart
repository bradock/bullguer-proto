import 'package:flutter/material.dart';
import 'package:protoburguer/main.dart';

class Carda extends StatefulWidget {
  @override
  _CardaState createState() => _CardaState();
}

class _CardaState extends State<Carda> {
  void trocarPrincipal(String imagePrato, String nomePrato, String precoPrato,
      String descricao) {
    setState(() {
      namePrincipal = nomePrato;
      pricePrincipal = precoPrato;
      imagePrincipal = imagePrato;
      describePrincipal = descricao;
    });
  }

  Map comida = {
    "name": "Bullrger\nSize I\n",
    "price": "R\$: 20:00",
    "image": "images/1.png",
    "namePequeno": "Bullrger Size I",
    "describe":
        "Lorem Ipsum é \nsimplesmen-te \numa simula\nção de texto da \nindústria \ntipográfica e de \nimpressos"
  };

  Map comida2 = {
    "name": "Bullrger\nSize II\n",
    "price": "R\$: 30:00",
    "image": "images/2.png",
    "namePequeno": "Bullrger Size II",
    "describe":
        "Lorem Ipsum é \nsimplesmen-te \numa simula\nção de texto da \nindústria \ntipográfica e de \nimpressos"
  };

  Map comida3 = {
    "name": "Bullrger\nSize III\n",
    "price": "R\$: 40:00",
    "image": "images/3.png",
    "namePequeno": "Bullrger Size III",
    "describe":
        "Lorem Ipsum é \nsimplesmen-te \numa simula\nção de texto da \nindústria \ntipográfica e de \nimpressos"
  };

  Map comida4 = {
    "name": "Bullrger\nSize IV\n",
    "price": "R\$: 50:00",
    "image": "images/4.png",
    "namePequeno": "Bullrger Size IV",
    "describe":
        "Lorem Ipsum é \nsimplesmen-te \numa simula\nção de texto da \nindústria \ntipográfica e de \nimpressos"
  };

  String namePrincipal = "Bullrger\nSize I\n";
  String pricePrincipal = "R\$: 20:00";
  String imagePrincipal = "images/1.png";
  String describePrincipal =
      "Lorem Ipsum é \nsimplesmen-te \numa simula\nção de texto da \nindústria \ntipográfica e de \nimpressos";

  List<dynamic> tt = [];

  Map bebida = {"name": "Coca-cola", "price": "R\$: 4.00"};

  Map bebida2 = {"name": "H2O", "price": "R\$: 3.00"};
  Map bebida3 = {"name": "Cerveja", "price": "R\$: 6.00"};
  Map bebida4 = {"name": "Dolly", "price": "R\$: 1.00"};
  Map bebida5 = {"name": "RedBull", "price": "R\$: 9.00"};

  List<dynamic> bebidas = [];

  @override
  Widget build(BuildContext context) {
    tt.add(comida);
    tt.add(comida2);
    tt.add(comida3);
    tt.add(comida4);

    bebidas.add(bebida);
    bebidas.add(bebida3);
    bebidas.add(bebida4);
    bebidas.add(bebida5);
    bebidas.add(bebida2);
    //print(tt[1]['name']);
    return Scaffold(
        appBar: AppBar(
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20),
                child: GestureDetector(
                  child: Icon(
                    Icons.monetization_on,
                    color: Colors.red,
                  ),
                  onTap: () {
                    Navigator.push((context),
                        new MaterialPageRoute(builder: (context) => MyApp()));
                  },
                ),
              )
            ],
            title: Image.asset('images/logo.png'),
            centerTitle: true,
            backgroundColor: Colors.black),
        body: Column(children: <Widget>[
          Container(
            height: 260,
            child: Stack(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: 100,
                    color: Colors.red[200]),
                Positioned(
                  top: 30,
                  left: 30,
                  child: Container(
                      //color: Colors.pink,
                      width: MediaQuery.of(context).size.width / 1.2,
                      height: 200,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Stack(children: <Widget>[
                              ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Image.asset(imagePrincipal)),
                              Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Container(
                                    child: Text(pricePrincipal,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                        textAlign: TextAlign.center),
                                    width: 60,
                                    height: 15,
                                    decoration: BoxDecoration(
                                        color: Colors.red[500],
                                        borderRadius: BorderRadius.circular(7)),
                                  ))
                            ]),
                            Padding(
                              padding: EdgeInsets.all(10),
                              child: Column(children: <Widget>[
                                Text(namePrincipal,
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold)),
                                Text(describePrincipal,
                                    style: TextStyle(fontSize: 10),
                                    textAlign: TextAlign.justify)
                              ]),
                            )
                          ])),
                ),
              ],
            ),
          ),
          Padding(
              padding: EdgeInsets.only(right: 200),
              child: Text("OFERTAS ESPECIAIS", style: TextStyle(fontSize: 13))),
          Expanded(
            flex: 3,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: tt.length,
              itemBuilder: (context, indece) {
                return buildListElement(tt[indece]['image'], tt[indece]['namePequeno'],
                    tt[indece]['price'], tt[indece]['describe'],tt[indece]['name']);
              },
            ),
          ),
          Expanded(
            flex: 1,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: bebidas.length,
                itemBuilder: (context, indice) {
                  return buildListDrink(
                      bebidas[indice]['name'], bebidas[indice]['price']);
                }),
          ),
        ]));
  }

  Widget buildListDrink(String nomeBebida, String precoBebida) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Stack(
        children: <Widget>[
          Container(
            width: 140,
            height: 23,
            child: Padding(
              padding: EdgeInsets.only(left: 12, top: 3),
              child: Text(
                nomeBebida,
                textAlign: TextAlign.start,
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20), color: Colors.red),
          ),
          Positioned(
            left: 90,
            child: Container(
              width: 40,
              height: 15,
              color: Colors.black,
              child: Text(
                precoBebida,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 11),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildListElement(String imagePrato, String nomePrato,
      String precoPrato, String descricao, String nomePratoPrincipal) {
    return Padding(
        padding: EdgeInsets.all(20),
        child: GestureDetector(
          onTap: () {
            trocarPrincipal(imagePrato, nomePratoPrincipal, precoPrato, descricao);
          },
          child: Stack(children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.asset(
                  imagePrato,
                  height: 170,
                  width: 170,
                )),
            Padding(
              padding: EdgeInsets.only(left: 40, top: 10),
              child: Container(
                  child: Text(nomePrato,
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                      textAlign: TextAlign.center),
                  width: 100,
                  height: 18,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20))),
            ),
            Padding(
              padding: EdgeInsets.only(left: 50, top: 140),
              child: Container(
                  child: Text(precoPrato,
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      textAlign: TextAlign.center),
                  width: 70,
                  height: 14,
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(20))),
            )
          ]),
        ));
  }
}
