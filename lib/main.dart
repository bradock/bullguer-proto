import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:protoburguer/carda.dart';

void main() async {
  //Firestore.instance.collection("teste").document("06543659114").setData({"data":"data3"});
  var now = new DateTime.now();
  //Firestore.instance.collection("cupons").document("06543659114").setData({
  //"info": [
  //{"nome": "cupom", "ativo": true, "data": now, "emissor": "Lucas"},
  //{"nome": "cupom", "ativo": true, "data": now, "emissor": "Cleiton"},
  //{"nome": "cupom", "ativo": false, "data": now, "emissor": "Hugo"},
  // {"nome": "cupom", "ativo": true, "data": now, "emissor": "Cleiton"}
  //]
    //});

  //Firestore.instance.collection("cupons").snapshots().listen((snap) {
  //for (DocumentSnapshot doce in snap.documents) {
  //  print(doce.data);
  //}
  //});

  runApp(MaterialApp(
      title: "teste Burgue",
      debugShowCheckedModeBanner: false,
      //home: MyApp(),
      home: Carda(),
      theme: ThemeData(
        //primarySwatch: Colors.black,
        primaryColor: Colors.red,
        primaryColorBrightness: Brightness.light,
      )));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  TextEditingController tt = TextEditingController();
  List<dynamic> bb = [];
  int totalCupom = 0;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void addCupom() async {
    var cpf = tt.text;
    bb = [];
    DocumentSnapshot doc =
        await Firestore.instance.collection("cupons").document(cpf).get();
    //print(doc.data);
    setState(() {
      if (doc.data != null) {
        for (final i in doc.data['info']) {
          bb.add(i);
          if (i['ativo'] == true) {
            totalCupom++;
          }
        }
      } else {
        totalCupom = 0;
      }
    });
    //print(bb);
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: (){
            Navigator.push(context, new MaterialPageRoute(
              builder: (context) => Carda()
            ));
          },
          child: Icon(Icons.home),
          backgroundColor: Colors.red[200] 
        ),
        appBar: AppBar(
            title: Image.asset("images/logo.png"),
            centerTitle: true,
            backgroundColor: Colors.black),
        body: Column(children: <Widget>[
          Container(
            height: 70,
            width: MediaQuery.of(context).size.width,
            color: Colors.redAccent[100],
            child: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Center(
                    child: Text(
                  "A CADA COMPRA REALIZADA, VOCÊ GANHA UM CUPOM NO APP, NA NONA COMPRA REGISTRADA COM CPF, VOCÊ GANHA UM BRINDE ESPECIAL.",
                  style: TextStyle(color: Colors.black87, fontSize: 11),
                  textAlign: TextAlign.center,
                ))),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: TextFormField(
                controller: tt,
                validator: (value) {
                  if (value.isEmpty) {
                    setState(() {
                      bb = [];
                      totalCupom = 0;
                    });
                    return "Digite o número do CPF!";
                  }
                },
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: "CPF", icon: Icon(Icons.fastfood)),
                style: TextStyle(color: Colors.black45, fontSize: 20.00),
              ),
            ),
          ),
          RaisedButton(
            onPressed: () {
              if (_formKey.currentState.validate()) {
                addCupom();
              }
            },
            child:
                Text("Verificar Cupons", style: TextStyle(color: Colors.white)),
            color: Colors.red[300],
          ),
          Expanded(
              child: Padding(
            padding: EdgeInsets.all(20.00),
            child: ListView.builder(
              itemCount: bb.length,
              itemBuilder: (context, item) {
                return ListTile(
                  title: (bb[item]['ativo'])
                      ? Text("Cupom Disponível")
                      : Text("Cupom Indisponível"),
                  subtitle: Text(
                      "Data: ${bb[item]['data']}\nPor: ${bb[item]['emissor']}"),
                  leading: (bb[item]['ativo'])
                      ? Icon(Icons.check_circle_outline)
                      : Icon(Icons.do_not_disturb),
                  selected: (bb[item]['ativo']) ? true : false,
                );
              },
            ),
          )),
          Padding(
              padding: EdgeInsets.all(20),
              child: Text(
                "Total de Cupom válido: ${totalCupom}",
                style: TextStyle(fontSize: 15),
              )),
        ]));
  }
}
